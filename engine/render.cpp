#ifndef RENDER_CPP_INCLUDED
#define RENDER_CPP_INCLUDED

#include<iostream>
#include<functional>
#include<SFML/Graphics.hpp> 
#include<SFML/System.hpp> 
#include"../misc/objects.cpp"

using namespace sf;

int render_window(RenderWindow *window, Objects *obj){
  window->clear(Color::Black);

  /* Render player */
  window->draw(obj->player->getShape());
  window->display();
  
  return 0;
}
#endif
