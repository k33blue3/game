#ifndef EVENTS_HPP_INCLUDED
#define EVENTS_HPP_INCLUDED
#include<iostream>
#include<functional>
#include<SFML/Graphics.hpp> 
#include<SFML/System.hpp> 
#include"../misc/npc.cpp"

using namespace sf;
int handle_events(RenderWindow *window, Objects *obj){
  Event event;

  /* Window manager events*/
  while (window->pollEvent(event)){
      if (event.type == Event::Closed)
	window->close();
  }

  /* Keyboard events */
  obj->player->controlPlayer();
 
  
  return 0;
}

#endif
