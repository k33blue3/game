#include<iostream>
#include<functional>
#include<SFML/Graphics.hpp> 
#include<SFML/System.hpp> 

using namespace sf;

#include "settings.h" 
#include "misc/player.cpp"
#include "misc/objects.cpp"
#include "engine/events.cpp"
#include "engine/logic.cpp"
#include "engine/render.cpp"


int main(int argc,  char *argv[]){

  RenderWindow window(VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT), TITLE);
  /* Player */
  Player player(50, 50);
  Objects objects(&player);
  /* Initiate the threads */ 
  //Thread logic_thread(std::bind(&game_logic, &window, &player));
  //Thread rendering_thread(std::bind(&render_window, &window, &player));
  

  /* Game loop */
  while(window.isOpen()){
    /////// THREADS ////////
    //logic_thread.launch();              /* Launch the logic handeler in its own thread. */
    //rendering_thread.launch();          /* Launch the renderer in its own thread. */
    game_logic(&window, &objects);
    render_window(&window, &objects);
    handle_events(&window, &objects);    /* Launch the events handeler in the main thread. */
    

  }
  

  return 0;
}
