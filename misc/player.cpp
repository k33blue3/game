#ifndef PLAYER_H_INCLUDED
#define PLAYER_H_INCLUDED

#include<iostream>
#include<SFML/Graphics.hpp> 
#include<SFML/System.hpp> 

#include "objects.cpp"

using namespace sf;

class Player{

 public:
  Vector2d *position;
  Color *color;      
  CircleShape *shape;
  float speed = 100/60; 
  
  Player(int x, int y){
    position = new Vector2d(x, y);
    shape = new CircleShape(20.f);
    shape->setFillColor(Color(1,200,1));
    color = new Color(10, 100, 10);
  }

  CircleShape getShape(){
    return *this->shape;
  }

  Vector2d getPosition(){
    return *this->position;
  }
  /* Move player relative to current position */
  int relativeMove(int x, int y){
    position->x += x * speed;
    position->y += y * speed;
    update_player();
  }

  void controlPlayer(){
    /* Player control */
    
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)){
      this->relativeMove(-1, 0);
    }if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)){
      this->relativeMove(1, 0);
    }if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)){
      this->relativeMove(0, -1);
    }if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)){
      this->relativeMove(0, 1);
    }
  }

  
  /* Update the shape position */
  void update_player(){
    this->shape->setPosition(Vector2f((float)position->x, (float)position->y));
  }

  /* Deallocate all memory */
  ~Player(){
    delete shape;
    delete color;
    delete position; 
  }
  
};

#endif
