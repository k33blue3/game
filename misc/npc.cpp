#ifndef NPC_H_INCLUDED
#define NPC_H_INCLUDED

#include<iostream>
#include<SFML/Graphics.hpp> 
#include<SFML/System.hpp> 
#include<math.h>
#include<cmath>

#include "objects.cpp"
#include "player.cpp"
#include "../settings.h"


using namespace sf;
class Character;
class Soldier{
 public:
  CircleShape *shape;
  int health = 100;
  float width = 20.f;
  const float speed = 1.f;
  Vector2d *position;
  int rotation = 180;
  const int spot_distance = 40;
  
  Soldier(int x, int y){
    shape = new CircleShape(width);
    shape->setFillColor(Color(1,1,200));
    position = new Vector2d(x, y);
  }
  
  ~Soldier(){
    delete this->shape;
    delete this->position;
  }

  /* Is the Soldier able to se said person? */
  bool canSee(Player  *person){
    Vector2d person_position  = person->getPosition();  
    float  distance = sqrt(pow(abs(person_position.x - position->x), 2) + pow(abs(person_position.y - position->y), 2));
    if (distance <= spot_distance) return true;
    else return false;
      
  }
  
  CircleShape getShape(){
    return *this->shape;
  }
  
  Vector2d getPosition(){
    return *this->position;
  }
  
  void moveTowards(Player *player = NULL, Soldier *soldier = NULL){
      /* Move towards the selected soldier */ 
    if ((player == NULL) and (soldier != NULL)){
      if (this->position->x > soldier->position->x){
	this->position->x -= this->speed;
      }if (this->position->x < soldier->position->x){
	this->position->x += this->speed;
      }if (this->position->y > soldier->position->y){
	this->position->y -= this->speed;
      }if (this->position->y < soldier->position->y){
	this->position->y += this->speed;
      }
    }
    /* Move towards the selected player */
    if ((player != NULL) and (soldier == NULL)){
      if (this->position->x > player->position->x){
	this->position->x -= this->speed;
      }if (this->position->x < player->position->x){
	this->position->x += this->speed;
      }if (this->position->y > player->position->y){
	this->position->y -= this->speed;
      }if (this->position->y < player->position->y){
	this->position->y += this->speed;
      }
      }
  }
  
};

class Character{
  /* 
     Uniform way of attacking living objects. 
   */
  
  Soldier *soldier;
  Player *player;
  short characterType;
  
  /* Assign the character */
  Character(Soldier *soldier = NULL, Player *player = NULL){
    if (soldier != NULL){
      this->soldier = soldier;
      this->characterType = SOLDIER;
    }
    else if (player != NULL){
      this->player = player;
      this->characterType = PLAYER;
    }
  }
  
};

class Swarm{
  std::vector<Character> *characterSwarm;
  Character *target;
  
  void updateSwarm(){


  }
  
  Swarm(Character *target){
    this->target = target;
  }
};

class Objects{
public:
  /* Containing all enemy swarms */
  std::vector<Swarm*> swarms;
  Player *player;

  Objects(Player *player){
    this->player = player;
  }

  void addSwarm(Swarm *swarm){
    this->swarms.at(this->swarms.size() - 1) = swarm;
  }
  
  ~Objects(){

  }
};


#endif
