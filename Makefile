LIBS = -lsfml-graphics -lsfml-window -lsfml-system 

Binary_name=main
Compiler=g++
CFLAGS=-Wall -std=c++11



all: main


main: main.o player.o objects.o npc.o
	$(Compiler) -g bin/main.o bin/player.o bin/objects.o bin/npc.o  -o bin/$(Binary_name) $(LIBS) 

player.o: misc/player.cpp
	$(Compiler) -c misc/player.cpp -o bin/player.o

objects.o: misc/objects.cpp
	$(Compiler) -c misc/objects.cpp -o bin/objects.o

npc.o: misc/npc.cpp
	$(Compiler) -c misc/npc.cpp -o bin/npc.o

main.o: main.cpp
	$(Compiler) -c main.cpp -o bin/main.o

clean:
	@echo "** REMOVING THE GAME ** "   
	rm bin/$(Binary_name)

install:
	@echo "** INSTALLING ..."
	cp bin/$(Binary_name) /usr/bin

uninstall:
	@echo "** UNINSTALLING..."
	rm /usr/bin/$(Binary_name)
